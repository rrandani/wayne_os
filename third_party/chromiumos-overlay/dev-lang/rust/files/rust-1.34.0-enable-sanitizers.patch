Sanitizer runtimes are currently behind the `-Z` flag, which is only available
on nightly builds of the compiler.  We would like to enable fuzzing on chrome
os anyway so move the sanitizer option under `-C` instead since we don't build
the toolchain in nightly mode.


diff --git a/src/librustc/session/config.rs b/src/librustc/session/config.rs
index d3a734cbc6..d8ae6a572f 100644
--- a/src/librustc/session/config.rs
+++ b/src/librustc/session/config.rs
@@ -1202,6 +1202,8 @@ options! {CodegenOptions, CodegenSetter, basic_codegen_options,
         "allow the linker to link its default libraries"),
     linker_flavor: Option<LinkerFlavor> = (None, parse_linker_flavor, [UNTRACKED],
                                            "Linker flavor"),
+    sanitizer: Option<Sanitizer> = (None, parse_sanitizer, [TRACKED],
+                                    "Use a sanitizer"),
     linker_plugin_lto: LinkerPluginLto = (LinkerPluginLto::Disabled,
         parse_linker_plugin_lto, [TRACKED],
         "generate build artifacts that are compatible with linker-based LTO."),
@@ -1376,8 +1378,6 @@ options! {DebuggingOptions, DebuggingSetter, basic_debugging_options,
         "take the breaks off const evaluation. NOTE: this is unsound"),
     osx_rpath_install_name: bool = (false, parse_bool, [TRACKED],
         "pass `-install_name @rpath/...` to the macOS linker"),
-    sanitizer: Option<Sanitizer> = (None, parse_sanitizer, [TRACKED],
-                                    "Use a sanitizer"),
     fuel: Option<(String, u64)> = (None, parse_optimization_fuel, [TRACKED],
         "set the optimization fuel quota for a crate"),
     print_fuel: Option<String> = (None, parse_opt_string, [TRACKED],
diff --git a/src/librustc_codegen_llvm/attributes.rs b/src/librustc_codegen_llvm/attributes.rs
index b15a64c966..9256c4052d 100644
--- a/src/librustc_codegen_llvm/attributes.rs
+++ b/src/librustc_codegen_llvm/attributes.rs
@@ -99,7 +99,7 @@ pub fn set_probestack(cx: &CodegenCx<'ll, '_>, llfn: &'ll Value) {
     // Currently stack probes seem somewhat incompatible with the address
     // sanitizer. With asan we're already protected from stack overflow anyway
     // so we don't really need stack probes regardless.
-    if let Some(Sanitizer::Address) = cx.sess().opts.debugging_opts.sanitizer {
+    if let Some(Sanitizer::Address) = cx.sess().opts.cg.sanitizer {
         return
     }
 
diff --git a/src/librustc_codegen_llvm/back/link.rs b/src/librustc_codegen_llvm/back/link.rs
index 6a3c2adc85..efef3e1453 100644
--- a/src/librustc_codegen_llvm/back/link.rs
+++ b/src/librustc_codegen_llvm/back/link.rs
@@ -491,7 +491,7 @@ fn link_natively(sess: &Session,
     cmd.args(&sess.opts.debugging_opts.pre_link_arg);
 
     if sess.target.target.options.is_like_fuchsia {
-        let prefix = match sess.opts.debugging_opts.sanitizer {
+        let prefix = match sess.opts.cg.sanitizer {
             Some(Sanitizer::Address) => "asan/",
             _ => "",
         };
diff --git a/src/librustc_codegen_llvm/declare.rs b/src/librustc_codegen_llvm/declare.rs
index 3febcb019c..e0edb655c8 100644
--- a/src/librustc_codegen_llvm/declare.rs
+++ b/src/librustc_codegen_llvm/declare.rs
@@ -50,7 +50,7 @@ fn declare_raw_fn(
         llvm::Attribute::NoRedZone.apply_llfn(Function, llfn);
     }
 
-    if let Some(ref sanitizer) = cx.tcx.sess.opts.debugging_opts.sanitizer {
+    if let Some(ref sanitizer) = cx.tcx.sess.opts.cg.sanitizer {
         match *sanitizer {
             Sanitizer::Address => {
                 llvm::Attribute::SanitizeAddress.apply_llfn(Function, llfn);
diff --git a/src/librustc_codegen_ssa/back/write.rs b/src/librustc_codegen_ssa/back/write.rs
index 4b02425d40..f0adbbdb0e 100644
--- a/src/librustc_codegen_ssa/back/write.rs
+++ b/src/librustc_codegen_ssa/back/write.rs
@@ -402,7 +402,7 @@ pub fn start_async_codegen<B: ExtraBackendMethods>(
     let mut metadata_config = ModuleConfig::new(vec![]);
     let mut allocator_config = ModuleConfig::new(vec![]);
 
-    if let Some(ref sanitizer) = sess.opts.debugging_opts.sanitizer {
+    if let Some(ref sanitizer) = sess.opts.cg.sanitizer {
         match *sanitizer {
             Sanitizer::Address => {
                 modules_config.passes.push("asan".to_owned());
diff --git a/src/librustc_metadata/creader.rs b/src/librustc_metadata/creader.rs
index 160d4c30c0..c4950f33ae 100644
--- a/src/librustc_metadata/creader.rs
+++ b/src/librustc_metadata/creader.rs
@@ -767,15 +767,23 @@ impl<'a> CrateLoader<'a> {
     }
 
     fn inject_sanitizer_runtime(&mut self) {
-        if let Some(ref sanitizer) = self.sess.opts.debugging_opts.sanitizer {
+        if let Some(ref sanitizer) = self.sess.opts.cg.sanitizer {
             // Sanitizers can only be used on some tested platforms with
             // executables linked to `std`
             const ASAN_SUPPORTED_TARGETS: &[&str] = &["x86_64-unknown-linux-gnu",
+                                                      "x86_64-cros-linux-gnu",
+                                                      "x86_64-pc-linux-gnu",
                                                       "x86_64-apple-darwin"];
             const TSAN_SUPPORTED_TARGETS: &[&str] = &["x86_64-unknown-linux-gnu",
+                                                      "x86_64-cros-linux-gnu",
+                                                      "x86_64-pc-linux-gnu",
                                                       "x86_64-apple-darwin"];
-            const LSAN_SUPPORTED_TARGETS: &[&str] = &["x86_64-unknown-linux-gnu"];
-            const MSAN_SUPPORTED_TARGETS: &[&str] = &["x86_64-unknown-linux-gnu"];
+            const LSAN_SUPPORTED_TARGETS: &[&str] = &["x86_64-unknown-linux-gnu",
+                                                      "x86_64-cros-linux-gnu",
+                                                      "x86_64-pc-linux-gnu"];
+            const MSAN_SUPPORTED_TARGETS: &[&str] = &["x86_64-unknown-linux-gnu",
+                                                      "x86_64-cros-linux-gnu",
+                                                      "x86_64-pc-linux-gnu"];
 
             let supported_targets = match *sanitizer {
                 Sanitizer::Address => ASAN_SUPPORTED_TARGETS,
@@ -794,7 +802,7 @@ impl<'a> CrateLoader<'a> {
             // firstyear 2017 - during testing I was unable to access an OSX machine
             // to make this work on different crate types. As a result, today I have
             // only been able to test and support linux as a target.
-            if self.sess.target.target.llvm_target == "x86_64-unknown-linux-gnu" {
+            if self.sess.target.target.llvm_target != "x86_64-apple-darwin" {
                 if !self.sess.crate_types.borrow().iter().all(|ct| {
                     match *ct {
                         // Link the runtime
