## SCOPE
1. Solving issues
2. Inspecting/Updating HW compatibility
3. Documentation
4. Redesign UI/UX
5. Bug fix





## RULES

- Format of the branch name

  [type]/[name]

  ​	ex) docs/README.md


- Format of the commit message

  [type] ([optional]) : [subject]

  [body]

  [footer]

  ​	ex) docs: Fix wrong spelled words in README


- Allowed [type] values
  - docs (change to the documentation)
  - uiux (redesign UI/UX)
  - hotfix (bug fix for the user, not a fix to a build script)





## HOW TO CONTRIBUTE

1. Register your own account on Gitlab.


2. Fork the project. you can find the fork button in the right side of the title of this project. by clicking the fork button, you can have same project on your own remote repository in your account.

 
3. Read [README]( https://gitlab.com/wayne-inc/wayne_os/blob/master/README.md ) and [CONTRIBUTING]( https://gitlab.com/wayne-inc/wayne_os/blob/master/CONTRIBUTING.md ) for understanding how to contribute to this project well. you need to follow the rules describing in [CONTRIBUTING]( https://gitlab.com/wayne-inc/wayne_os/blob/master/CONTRIBUTING.md ).

 
4. Here is the way.
 

   - Solving issues
   
     - Check out issues on the Wayne_OS project. try to understand what issues are at first and search for the solution, discuss with the other contributors or the maintainer of this project.
    <br/>

   - Inspecting/Updating HW compatibility
    
     - Download the installer and install Wayne OS on the prepared disk at first. you can find how to do it in [README]( https://gitlab.com/wayne-inc/wayne_os/blob/master/README.md ) or https://www.wayne-inc.com. If booting is successful, Please write HW specification in the table on [README]( https://gitlab.com/wayne-inc/wayne_os/blob/master/README.md ). before writing it, create a new branch from master branch following the rule. you can create a new branch on gitlab website. then, edit [Wayne-OS-HW-Compatibility]( https://gitlab.com/wayne-inc/wayne_os/blob/master/Wayne-OS-HW-Compatibility.md ) for filling out the table with HW specification you tried.
    <br/>

   - Documentation
  
     - When you find out something is wrong such as wrong spelled words, lack of information, etc. in [README]( https://gitlab.com/wayne-inc/wayne_os/blob/master/README.md ) or [CONTRIBUTING]( https://gitlab.com/wayne-inc/wayne_os/blob/master/CONTRIBUTING.md ), you can fix it for other users or contributors. create a new branch from master branch following the rule. then, edit [README](https://gitlab.com/wayne-inc/wayne_os/blob/master/README.md) or [CONTRIBUTING]( https://gitlab.com/wayne-inc/wayne_os/blob/master/CONTRIBUTING.md ).
    <br/>

   - Redesign UI/UX
   
     - you may be difficult to modify UI/UX on gitlab website. so download and install Git. you can download it in [here]( https://git-scm.com/ ). you can also learn all about Git on that site. after that, clone the project on your local repository. create a new branch from master branch following the rule. then, redesign UI/UX for this project with your skill.
    <br/>

   - Bug fix
    
     - Before fixing bugs, create an issue on this project to notify other contributors and the maintainer of this project. by this way, other contributors and the maintainer of this project can help you. you can fix bugs easier than when you try to fix bugs alone. clone the project on your local repository and create a new branch from master branch following the rule. then, try to fix bugs discussing other contributors or the maintainer of this project.
    <br/>


5. Before pushing the commit, you have to make sure that It has no problem. test your work whether it is working well. you have to push the commit for your work when it is the stable version.


6. Create a merge request for implementing your work on this project. when you send a merge request, leave a well-defined comment that describes what you did and difference between before and after.


   - Inspecting/Updating HW compatibility and Documentation
   
     - send a merge request to Documentation branch on wayne-inc/wayne_os
    <br/>

   - Redesign UI/UX
 
     - send a merge request to uiux branch on wayne-inc/wayne_os
    <br/>

   - Bug fix
   
     - send a merge request to master branch on wayne-inc/wayne_os
    <br/>


7. Review and discuss about your work with the maintainer of this project.


8. If maintainer want to accept your work on this project, they will merge your work on the official version.





## QUESTION

 If you want to ask for anything about this project, please contact with below address.

e-mail: ainesh@wayne-inc.com
