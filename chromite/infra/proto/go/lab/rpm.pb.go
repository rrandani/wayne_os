// Code generated by protoc-gen-go. DO NOT EDIT.
// source: lab/rpm.proto

package lab

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

// Remote power management info.
// NEXT TAG: 3
type RPM struct {
	PowerunitName        string   `protobuf:"bytes,1,opt,name=powerunit_name,json=powerunitName,proto3" json:"powerunit_name,omitempty"`
	PowerunitOutlet      string   `protobuf:"bytes,2,opt,name=powerunit_outlet,json=powerunitOutlet,proto3" json:"powerunit_outlet,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *RPM) Reset()         { *m = RPM{} }
func (m *RPM) String() string { return proto.CompactTextString(m) }
func (*RPM) ProtoMessage()    {}
func (*RPM) Descriptor() ([]byte, []int) {
	return fileDescriptor_f0a094727c1a4149, []int{0}
}

func (m *RPM) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_RPM.Unmarshal(m, b)
}
func (m *RPM) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_RPM.Marshal(b, m, deterministic)
}
func (m *RPM) XXX_Merge(src proto.Message) {
	xxx_messageInfo_RPM.Merge(m, src)
}
func (m *RPM) XXX_Size() int {
	return xxx_messageInfo_RPM.Size(m)
}
func (m *RPM) XXX_DiscardUnknown() {
	xxx_messageInfo_RPM.DiscardUnknown(m)
}

var xxx_messageInfo_RPM proto.InternalMessageInfo

func (m *RPM) GetPowerunitName() string {
	if m != nil {
		return m.PowerunitName
	}
	return ""
}

func (m *RPM) GetPowerunitOutlet() string {
	if m != nil {
		return m.PowerunitOutlet
	}
	return ""
}

func init() {
	proto.RegisterType((*RPM)(nil), "lab.RPM")
}

func init() { proto.RegisterFile("lab/rpm.proto", fileDescriptor_f0a094727c1a4149) }

var fileDescriptor_f0a094727c1a4149 = []byte{
	// 149 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0xe2, 0xcd, 0x49, 0x4c, 0xd2,
	0x2f, 0x2a, 0xc8, 0xd5, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0x62, 0xce, 0x49, 0x4c, 0x52, 0x0a,
	0xe7, 0x62, 0x0e, 0x0a, 0xf0, 0x15, 0x52, 0xe5, 0xe2, 0x2b, 0xc8, 0x2f, 0x4f, 0x2d, 0x2a, 0xcd,
	0xcb, 0x2c, 0x89, 0xcf, 0x4b, 0xcc, 0x4d, 0x95, 0x60, 0x54, 0x60, 0xd4, 0xe0, 0x0c, 0xe2, 0x85,
	0x8b, 0xfa, 0x25, 0xe6, 0xa6, 0x0a, 0x69, 0x72, 0x09, 0x20, 0x94, 0xe5, 0x97, 0x96, 0xe4, 0xa4,
	0x96, 0x48, 0x30, 0x81, 0x15, 0xf2, 0xc3, 0xc5, 0xfd, 0xc1, 0xc2, 0x4e, 0xfa, 0x51, 0xba, 0xe9,
	0xf9, 0x7a, 0xc9, 0x19, 0x45, 0xf9, 0xb9, 0x99, 0xa5, 0xb9, 0x7a, 0xf9, 0x45, 0xe9, 0xfa, 0x30,
	0x4e, 0x7e, 0xb1, 0x7e, 0x66, 0x5e, 0x5a, 0x51, 0xa2, 0x3e, 0xd8, 0x1d, 0xfa, 0xe9, 0xf9, 0xfa,
	0x39, 0x89, 0x49, 0x49, 0x6c, 0x60, 0x9e, 0x31, 0x20, 0x00, 0x00, 0xff, 0xff, 0xea, 0x38, 0x64,
	0xb2, 0xa6, 0x00, 0x00, 0x00,
}
