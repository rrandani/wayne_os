// Code generated by protoc-gen-go. DO NOT EDIT.
// source: chromite/api/android.proto

package api

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	chromiumos "go.chromium.org/chromiumos/infra/proto/go/chromiumos"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type MarkStableStatusType int32

const (
	// Unspecified
	MarkStableStatusType_MARK_STABLE_STATUS_UNSPECIFIED MarkStableStatusType = 0
	// Success
	MarkStableStatusType_MARK_STABLE_STATUS_SUCCESS MarkStableStatusType = 1
	// Pinned (at android_atom)
	MarkStableStatusType_MARK_STABLE_STATUS_PINNED MarkStableStatusType = 2
	// Early exit
	MarkStableStatusType_MARK_STABLE_STATUS_EARLY_EXIT MarkStableStatusType = 3
)

var MarkStableStatusType_name = map[int32]string{
	0: "MARK_STABLE_STATUS_UNSPECIFIED",
	1: "MARK_STABLE_STATUS_SUCCESS",
	2: "MARK_STABLE_STATUS_PINNED",
	3: "MARK_STABLE_STATUS_EARLY_EXIT",
}

var MarkStableStatusType_value = map[string]int32{
	"MARK_STABLE_STATUS_UNSPECIFIED": 0,
	"MARK_STABLE_STATUS_SUCCESS":     1,
	"MARK_STABLE_STATUS_PINNED":      2,
	"MARK_STABLE_STATUS_EARLY_EXIT":  3,
}

func (x MarkStableStatusType) String() string {
	return proto.EnumName(MarkStableStatusType_name, int32(x))
}

func (MarkStableStatusType) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_f74c095a284c1bd1, []int{0}
}

type MarkStableRequest struct {
	// Required if not default.
	// The chroot where.
	Chroot *chromiumos.Chroot `protobuf:"bytes,1,opt,name=chroot,proto3" json:"chroot,omitempty"`
	// Required.
	// The manifest branch being used.
	TrackingBranch string `protobuf:"bytes,2,opt,name=tracking_branch,json=trackingBranch,proto3" json:"tracking_branch,omitempty"`
	// Required.
	// Portage package name for Android container.
	PackageName string `protobuf:"bytes,3,opt,name=package_name,json=packageName,proto3" json:"package_name,omitempty"`
	// Required.
	// Android branch to import from.
	AndroidBuildBranch string `protobuf:"bytes,4,opt,name=android_build_branch,json=androidBuildBranch,proto3" json:"android_build_branch,omitempty"`
	// Force set the android build id that will be used.
	AndroidVersion string `protobuf:"bytes,5,opt,name=android_version,json=androidVersion,proto3" json:"android_version,omitempty"`
	// Android GTS branch to copy artifacts from.
	AndroidGtsBuildBranch string `protobuf:"bytes,6,opt,name=android_gts_build_branch,json=androidGtsBuildBranch,proto3" json:"android_gts_build_branch,omitempty"`
	// The set of relevant build targets. Used to clean old version and for a
	// emerge-able sanity check for the new version.
	// Recommended.
	BuildTargets         []*chromiumos.BuildTarget `protobuf:"bytes,7,rep,name=build_targets,json=buildTargets,proto3" json:"build_targets,omitempty"`
	XXX_NoUnkeyedLiteral struct{}                  `json:"-"`
	XXX_unrecognized     []byte                    `json:"-"`
	XXX_sizecache        int32                     `json:"-"`
}

func (m *MarkStableRequest) Reset()         { *m = MarkStableRequest{} }
func (m *MarkStableRequest) String() string { return proto.CompactTextString(m) }
func (*MarkStableRequest) ProtoMessage()    {}
func (*MarkStableRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_f74c095a284c1bd1, []int{0}
}

func (m *MarkStableRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_MarkStableRequest.Unmarshal(m, b)
}
func (m *MarkStableRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_MarkStableRequest.Marshal(b, m, deterministic)
}
func (m *MarkStableRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_MarkStableRequest.Merge(m, src)
}
func (m *MarkStableRequest) XXX_Size() int {
	return xxx_messageInfo_MarkStableRequest.Size(m)
}
func (m *MarkStableRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_MarkStableRequest.DiscardUnknown(m)
}

var xxx_messageInfo_MarkStableRequest proto.InternalMessageInfo

func (m *MarkStableRequest) GetChroot() *chromiumos.Chroot {
	if m != nil {
		return m.Chroot
	}
	return nil
}

func (m *MarkStableRequest) GetTrackingBranch() string {
	if m != nil {
		return m.TrackingBranch
	}
	return ""
}

func (m *MarkStableRequest) GetPackageName() string {
	if m != nil {
		return m.PackageName
	}
	return ""
}

func (m *MarkStableRequest) GetAndroidBuildBranch() string {
	if m != nil {
		return m.AndroidBuildBranch
	}
	return ""
}

func (m *MarkStableRequest) GetAndroidVersion() string {
	if m != nil {
		return m.AndroidVersion
	}
	return ""
}

func (m *MarkStableRequest) GetAndroidGtsBuildBranch() string {
	if m != nil {
		return m.AndroidGtsBuildBranch
	}
	return ""
}

func (m *MarkStableRequest) GetBuildTargets() []*chromiumos.BuildTarget {
	if m != nil {
		return m.BuildTargets
	}
	return nil
}

type MarkStableResponse struct {
	// Possible errors.
	Status MarkStableStatusType `protobuf:"varint,1,opt,name=status,proto3,enum=chromite.api.MarkStableStatusType" json:"status,omitempty"`
	// The new package atom.
	AndroidAtom          *chromiumos.PackageInfo `protobuf:"bytes,2,opt,name=android_atom,json=androidAtom,proto3" json:"android_atom,omitempty"`
	XXX_NoUnkeyedLiteral struct{}                `json:"-"`
	XXX_unrecognized     []byte                  `json:"-"`
	XXX_sizecache        int32                   `json:"-"`
}

func (m *MarkStableResponse) Reset()         { *m = MarkStableResponse{} }
func (m *MarkStableResponse) String() string { return proto.CompactTextString(m) }
func (*MarkStableResponse) ProtoMessage()    {}
func (*MarkStableResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_f74c095a284c1bd1, []int{1}
}

func (m *MarkStableResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_MarkStableResponse.Unmarshal(m, b)
}
func (m *MarkStableResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_MarkStableResponse.Marshal(b, m, deterministic)
}
func (m *MarkStableResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_MarkStableResponse.Merge(m, src)
}
func (m *MarkStableResponse) XXX_Size() int {
	return xxx_messageInfo_MarkStableResponse.Size(m)
}
func (m *MarkStableResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_MarkStableResponse.DiscardUnknown(m)
}

var xxx_messageInfo_MarkStableResponse proto.InternalMessageInfo

func (m *MarkStableResponse) GetStatus() MarkStableStatusType {
	if m != nil {
		return m.Status
	}
	return MarkStableStatusType_MARK_STABLE_STATUS_UNSPECIFIED
}

func (m *MarkStableResponse) GetAndroidAtom() *chromiumos.PackageInfo {
	if m != nil {
		return m.AndroidAtom
	}
	return nil
}

type UnpinVersionRequest struct {
	// The chroot to use to execute the endpoint.
	Chroot               *chromiumos.Chroot `protobuf:"bytes,1,opt,name=chroot,proto3" json:"chroot,omitempty"`
	XXX_NoUnkeyedLiteral struct{}           `json:"-"`
	XXX_unrecognized     []byte             `json:"-"`
	XXX_sizecache        int32              `json:"-"`
}

func (m *UnpinVersionRequest) Reset()         { *m = UnpinVersionRequest{} }
func (m *UnpinVersionRequest) String() string { return proto.CompactTextString(m) }
func (*UnpinVersionRequest) ProtoMessage()    {}
func (*UnpinVersionRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_f74c095a284c1bd1, []int{2}
}

func (m *UnpinVersionRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UnpinVersionRequest.Unmarshal(m, b)
}
func (m *UnpinVersionRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UnpinVersionRequest.Marshal(b, m, deterministic)
}
func (m *UnpinVersionRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UnpinVersionRequest.Merge(m, src)
}
func (m *UnpinVersionRequest) XXX_Size() int {
	return xxx_messageInfo_UnpinVersionRequest.Size(m)
}
func (m *UnpinVersionRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_UnpinVersionRequest.DiscardUnknown(m)
}

var xxx_messageInfo_UnpinVersionRequest proto.InternalMessageInfo

func (m *UnpinVersionRequest) GetChroot() *chromiumos.Chroot {
	if m != nil {
		return m.Chroot
	}
	return nil
}

type UnpinVersionResponse struct {
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *UnpinVersionResponse) Reset()         { *m = UnpinVersionResponse{} }
func (m *UnpinVersionResponse) String() string { return proto.CompactTextString(m) }
func (*UnpinVersionResponse) ProtoMessage()    {}
func (*UnpinVersionResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_f74c095a284c1bd1, []int{3}
}

func (m *UnpinVersionResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UnpinVersionResponse.Unmarshal(m, b)
}
func (m *UnpinVersionResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UnpinVersionResponse.Marshal(b, m, deterministic)
}
func (m *UnpinVersionResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UnpinVersionResponse.Merge(m, src)
}
func (m *UnpinVersionResponse) XXX_Size() int {
	return xxx_messageInfo_UnpinVersionResponse.Size(m)
}
func (m *UnpinVersionResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_UnpinVersionResponse.DiscardUnknown(m)
}

var xxx_messageInfo_UnpinVersionResponse proto.InternalMessageInfo

func init() {
	proto.RegisterEnum("chromite.api.MarkStableStatusType", MarkStableStatusType_name, MarkStableStatusType_value)
	proto.RegisterType((*MarkStableRequest)(nil), "chromite.api.MarkStableRequest")
	proto.RegisterType((*MarkStableResponse)(nil), "chromite.api.MarkStableResponse")
	proto.RegisterType((*UnpinVersionRequest)(nil), "chromite.api.UnpinVersionRequest")
	proto.RegisterType((*UnpinVersionResponse)(nil), "chromite.api.UnpinVersionResponse")
}

func init() { proto.RegisterFile("chromite/api/android.proto", fileDescriptor_f74c095a284c1bd1) }

var fileDescriptor_f74c095a284c1bd1 = []byte{
	// 563 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x9c, 0x94, 0xdf, 0x8e, 0xd2, 0x40,
	0x14, 0xc6, 0x2d, 0x28, 0x1b, 0x0f, 0x08, 0x75, 0x44, 0xb7, 0x36, 0xee, 0x0a, 0xbd, 0x59, 0xb2,
	0x17, 0xad, 0xc1, 0x44, 0xcd, 0xc6, 0x9b, 0xc2, 0x56, 0x43, 0xdc, 0x25, 0xa4, 0x05, 0xff, 0x25,
	0xa6, 0x19, 0xca, 0x6c, 0xb7, 0x61, 0xdb, 0xa9, 0xed, 0xb0, 0x89, 0xef, 0xe0, 0x23, 0xf8, 0x34,
	0xfb, 0x16, 0x5e, 0xf8, 0x08, 0xfb, 0x0e, 0x86, 0x99, 0x41, 0x8a, 0x61, 0xbd, 0xf0, 0x8a, 0x70,
	0xbe, 0xdf, 0x77, 0x66, 0xce, 0x77, 0x32, 0x05, 0x3d, 0x38, 0xcf, 0x68, 0x1c, 0x31, 0x62, 0xe1,
	0x34, 0xb2, 0x70, 0x32, 0xcb, 0x68, 0x34, 0x33, 0xd3, 0x8c, 0x32, 0x8a, 0x6a, 0x2b, 0xcd, 0xc4,
	0x69, 0xa4, 0x3f, 0xd9, 0x20, 0xa7, 0x8b, 0xe8, 0x62, 0xe6, 0xe3, 0x34, 0x12, 0xac, 0xbe, 0x2b,
	0xd4, 0x45, 0x4c, 0x73, 0x2b, 0xa0, 0x71, 0x4c, 0x13, 0x21, 0x18, 0xbf, 0x4a, 0x70, 0xff, 0x14,
	0x67, 0x73, 0x8f, 0xe1, 0xe9, 0x05, 0x71, 0xc9, 0xd7, 0x05, 0xc9, 0x19, 0x3a, 0x84, 0xca, 0xd2,
	0x40, 0x99, 0xa6, 0xb4, 0x94, 0x4e, 0xb5, 0x8b, 0xcc, 0xb5, 0xdf, 0xec, 0x73, 0xc5, 0x95, 0x04,
	0x3a, 0x80, 0x06, 0xcb, 0x70, 0x30, 0x8f, 0x92, 0xd0, 0x9f, 0x66, 0x38, 0x09, 0xce, 0xb5, 0x52,
	0x4b, 0xe9, 0xdc, 0x75, 0xeb, 0xab, 0x72, 0x8f, 0x57, 0x51, 0x1b, 0x6a, 0x29, 0x0e, 0xe6, 0x38,
	0x24, 0x7e, 0x82, 0x63, 0xa2, 0x95, 0x39, 0x55, 0x95, 0xb5, 0x21, 0x8e, 0x09, 0x7a, 0x06, 0x4d,
	0x39, 0xa3, 0x2f, 0x26, 0x90, 0x0d, 0x6f, 0x73, 0x14, 0x49, 0xad, 0xb7, 0x94, 0x64, 0xd3, 0x03,
	0x68, 0xac, 0x1c, 0x97, 0x24, 0xcb, 0x23, 0x9a, 0x68, 0x77, 0xc4, 0xe9, 0xb2, 0xfc, 0x5e, 0x54,
	0xd1, 0x4b, 0xd0, 0x56, 0x60, 0xc8, 0xf2, 0xcd, 0xf6, 0x15, 0xee, 0x78, 0x28, 0xf5, 0xb7, 0x2c,
	0x2f, 0x9e, 0xf0, 0x1a, 0xee, 0x09, 0x98, 0xe1, 0x2c, 0x24, 0x2c, 0xd7, 0x76, 0x5a, 0xe5, 0x4e,
	0xb5, 0xbb, 0x5b, 0x8c, 0x84, 0xf3, 0x63, 0xae, 0xbb, 0xb5, 0xe9, 0xfa, 0x4f, 0x6e, 0x7c, 0x57,
	0x00, 0x15, 0xf3, 0xcd, 0x53, 0x9a, 0xe4, 0x04, 0x1d, 0x41, 0x25, 0x67, 0x98, 0x2d, 0x72, 0x1e,
	0x70, 0xbd, 0x6b, 0x98, 0xc5, 0x65, 0x9a, 0x6b, 0x87, 0xc7, 0xa9, 0xf1, 0xb7, 0x94, 0xb8, 0xd2,
	0x81, 0x8e, 0xa0, 0xb6, 0x9a, 0x04, 0x33, 0x1a, 0xf3, 0xb4, 0xff, 0xba, 0xcf, 0x48, 0x64, 0x3a,
	0x48, 0xce, 0xa8, 0x5b, 0x95, 0xb0, 0xcd, 0x68, 0x6c, 0xd8, 0xf0, 0x60, 0x92, 0xa4, 0x51, 0x22,
	0x53, 0xf9, 0x8f, 0x7d, 0x1b, 0x8f, 0xa0, 0xb9, 0xd9, 0x42, 0x8c, 0x74, 0xf8, 0x43, 0x81, 0xe6,
	0xb6, 0x7b, 0x23, 0x03, 0xf6, 0x4f, 0x6d, 0xf7, 0x9d, 0xef, 0x8d, 0xed, 0xde, 0x89, 0xb3, 0xfc,
	0x19, 0x4f, 0x3c, 0x7f, 0x32, 0xf4, 0x46, 0x4e, 0x7f, 0xf0, 0x66, 0xe0, 0x1c, 0xab, 0xb7, 0xd0,
	0x3e, 0xe8, 0x5b, 0x18, 0x6f, 0xd2, 0xef, 0x3b, 0x9e, 0xa7, 0x2a, 0x68, 0x0f, 0x1e, 0x6f, 0xd1,
	0x47, 0x83, 0xe1, 0xd0, 0x39, 0x56, 0x4b, 0xa8, 0x0d, 0x7b, 0x5b, 0x64, 0xc7, 0x76, 0x4f, 0x3e,
	0xf9, 0xce, 0xc7, 0xc1, 0x58, 0x2d, 0x77, 0x7f, 0x2a, 0x50, 0xb7, 0x45, 0x12, 0x1e, 0xc9, 0x2e,
	0xa3, 0x80, 0xa0, 0x0f, 0x00, 0xeb, 0x0b, 0xa3, 0xa7, 0x37, 0xad, 0x40, 0x86, 0xa4, 0xb7, 0x6e,
	0x06, 0x44, 0x04, 0x46, 0xe5, 0xea, 0x5a, 0x2f, 0xa9, 0x25, 0xf4, 0x05, 0x6a, 0xc5, 0x88, 0x50,
	0x7b, 0xd3, 0xb9, 0x65, 0x03, 0xba, 0xf1, 0x2f, 0x64, 0xa3, 0xbd, 0xa2, 0x37, 0xae, 0xae, 0xf5,
	0x2a, 0xec, 0xc8, 0xbd, 0xaa, 0x4a, 0xef, 0xd5, 0xe7, 0x17, 0x21, 0xfd, 0xb3, 0x32, 0x93, 0x66,
	0xa1, 0x55, 0x78, 0xef, 0x51, 0x72, 0x96, 0x61, 0x8b, 0x3f, 0x77, 0x2b, 0xa4, 0x56, 0xf1, 0x3b,
	0x31, 0xad, 0xf0, 0xf2, 0xf3, 0xdf, 0x01, 0x00, 0x00, 0xff, 0xff, 0x4d, 0x3d, 0x49, 0x44, 0x68,
	0x04, 0x00, 0x00,
}
