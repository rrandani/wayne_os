# Tast documentation (go/tast-docs)

This is a list of Tast documentation.
For more general information about Tast, see [README](../README.md).

## Tast tests

*   [Quickstart](quickstart.md)
*   [Running Tests](running_tests.md)
*   [Writing Tests](writing_tests.md)
*   [Getting Code Reviews](code_reviews.md)
*   [Codelab #1](codelab_1.md)
*   [Codelab #2](codelab_2.md)
*   [Test Attributes](test_attributes.md)
*   [Test Dependencies](test_dependencies.md)
*   [Test Validation](test_validation.md)
*   [Public test repository](https://chromium.googlesource.com/chromiumos/platform/tast-tests/)
*   [Video Tests](https://chromium.googlesource.com/chromiumos/platform/tast-tests/+/refs/heads/master/src/chromiumos/tast/local/bundles/cros/video/README.md)

## Tast framework

*   [Overview](overview.md)
*   [Design Principles](design_principles.md)
*   [Modifying Tast](modifying_tast.md)

## Integration with Chrome OS infrastructure (for Googlers)

*   [Adding new tests to Chrome OS infra](http://go/tast-add-test)
*   [Tast Infra Integration](http://go/tast-infra)
*   [Investigating Tast Test Failures](http://go/tast-failures)
