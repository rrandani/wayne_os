Chrome OS tidak ada atau rusak.
Harap masukkan stik USB atau kartu SD pemulihan.
Harap masukkan stik USB pemulihan.
Harap masukkan kartu SD atau stik USB pemulihan (catatan: port USB berwarna biru TIDAK akan berfungsi untuk pemulihan).
Harap masukkan stik USB ke salah satu dari 4 port di bagian BELAKANG perangkat.
Perangkat yang Anda masukkan tidak berisi Chrome OS.
Verifikasi OS NONAKTIF
Tekan SPACE untuk mengaktifkan kembali.
Tekan ENTER untuk mengonfirmasi bahwa Anda ingin mengaktifkan verifikasi OS.
Sistem Anda akan reboot dan data lokal akan dihapus.
Untuk kembali, tekan ESC.
Verifikasi OS AKTIF.
Untuk MENONAKTIFKAN verifikasi OS, tekan ENTER.
Untuk mendapat bantuan, buka https://google.com/chromeos/recovery
Kode error
Harap hapus semua perangkat eksternal untuk memulai pemulihan.
Model 60061e
Untuk MENONAKTIFKAN verifikasi OS, tekan tombol PEMULIHAN.
Daya pada sumber listrik yang terhubung tidak cukup untuk menjalankan perangkat ini.
Chrome OS akan dimatikan sekarang.
Harap gunakan adaptor yang tepat kemudian coba lagi.
Harap hapus semua perangkat yang terhubung lalu mulai pemulihan.
Tekan kunci numerik untuk memilih bootloader alternatif:
Tekan tombol POWER untuk menjalankan diagnostik.
Untuk MENONAKTIFKAN verifikasi OS, tekan tombol POWER.
