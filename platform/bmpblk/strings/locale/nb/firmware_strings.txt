Chrome OS mangler eller er skadet.
Sett inn en USB-minnepinne eller et SD-kort for gjenoppretting.
Sett inn en USB-minnepinne for gjenoppretting.
Sett inn et SD-kort eller en USB-minnepinne for gjenoppretting (merk: den blå USB-inngangen kan IKKE brukes for gjenoppretting).
Sett inn en USB-minnepinne for gjenoppretting i en av de fire portene på BAKSIDEN av enheten.
Enheten du har satt inn, inneholder ikke Chrome OS:
OS-bekreftelse er AV
Trykk på MELLOMROMSTASTEN for å slå på.
Trykk på ENTER for å bekrefte at du vil slå på OS-bekreftelse.
Systemet ditt kommer til å starte på nytt, og lokale data blir slettet.
For å gå tilbake, trykk på ESC.
OS-bekreftelse er PÅ.
For å slå OS-bekreftelse AV, trykk på ENTER.
For å få hjelp, gå til https://google.com/chromeos/recovery
Feilkode
Fjern alle eksterne enheter for å starte gjenoppretting.
Modell 60061e
For å slå OS-bekreftelse AV, trykk på GJENOPPRETTING-knappen.
Strømforsyningen som er tilkoblet, er for svak til å drive denne enheten.
Chrome OS avsluttes nå.
Bruk riktig adapter og prøv på nytt.
Fjern alle tilkoblede enheter, og begynn gjenopprettingen.
Trykk på en talltast for å velge en alternativ oppstartsinnlaster:
Trykk på av/på-knappen for å kjøre diagnostikk.
For å slå OS-bekreftelse AV, trykk på AV/PÅ-knappen.
