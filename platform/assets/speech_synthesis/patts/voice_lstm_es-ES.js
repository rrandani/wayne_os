// Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// AUTOGENERATED FILE
//
// This file is autogenerated! If you need to modify it, be sure to
// modify the script that exports Google voice data for use in Chrome.

// Initialize the voice array if it doesn't exist so that voice data files
// can be loaded in any order.

if (!window.voices) {
  window.voices = [];
}

// Add this voice to the global voice array.
window.voices.push({
  'pipelineFile': '/voice_lstm_es-ES/ana/pipeline',
  'prefix': '/voice_lstm_es-ES/ana/',
  'voiceType': 'lstm',
  'cacheToDisk': false,
  'lang': 'es-ES',
  'displayName': 'Spanish (Spain)',
  'voiceName': 'Chrome OS español',
  'removePaths': [],
  'files': [
    {
      'path': '/voice_lstm_es-ES.zvoice',
      'url': '',
      'md5sum': '66b9ccc8a7f7d060d66b3161d78c02ff',
      'size': 6358398,
    },
  ],
});
